Vue.component('editable', {
    template: '<div style="display: inline-block" contenteditable="true" @input="update"></div>',
    props: ['content'],
    mounted: function () {
        this.$el.innerText = this.content;
    },
    methods: {
        update: function (event) {
            this.$emit('update', event.target.innerText);
        }
    }
})
new Vue({
    el: "#app",
    data() {
        let d = {
            cards: [], // Menge aller Karten
            openCards: [], // aktuell aufgedeckte Karten; Karten in diesem Array werden als umgedreht angezeigt
            state: {type: 'beforeStart'}, // aktueller Spielzustand
            player: {}, // Menge aller Spieler
            foundCards: [], // Menge der bereits aufgedeckten Karten; Karten die in diesem Array sind werden nicht mehr auf dem Spielfeld angezeigt
            missmatchedCards: [], // Menge der aktuell falsch zugeordneten Karten (wird für eine Animation gebraucht)
            numberOfPlayers: 2,
            numberOfCards: 8, // Anzahl der unterschiedlichen Karten (auf dem Spielfeld werden doppelt so viele Karten sein)
            currentDuration: 0,
            timer: null,
        }
        for (let i = 0; i < d.numberOfPlayers; i++) {
            d.player[i] = {points: 0, name: `Spieler ${i+1}`, id: i}
        }
        for (let i = 0; i < d.numberOfCards; i++) {
            const imageUrl = 'card'+i+'.png';
            d.cards.push({ id: i, name: "Thing " + i, imageUrl: imageUrl });
            d.cards.push({ id: 8+i, name: "Thing " + i, imageUrl: imageUrl });
        }
        return d;
    },
    methods: {
        startGame() {
            this.state = {type: 'select', player: 1} // player: 1 bedeutet Spieler 1 ist am Zug
            this.shuffle();
            setTimeout(() => {
                this.shuffle();
                this.currentDuration = 0;
                this.timer = setInterval(() => {
                    this.currentDuration += 1;
                }, 1000);
            }, 500);
           
        },
        stopGame() {
            this.state = {type: 'beforeStart'}
            this.missmatchedCards = []
            for (let i in this.player) {
                this.player[i].points = 0
            }
            this.openCards = []
            this.foundCards = []
            if(this.timer) {
                clearInterval(this.timer);
            }
        },
        cardWasFound(card) {
            return this.foundCards.includes(card)
        },
        getPlayerName(id) {
            return (this.player[id] || {}).name
        },
        gameUpdate() {
            // state.type ist 'select', wenn ein spieler 2 Karten aufgewählt hat
            // die Karten sind dann in this.openCards gespeichert
            if (this.state.type === 'select') {
                const doCardsMatch = this.openCards[0].name === this.openCards[1].name

                this.missmatchedCards = []
                // gebe dem Nutzer zeit die Karten anzuschauen
                setTimeout(() => {
                    if (doCardsMatch) {
                        //Karten passen zusammen -> gebe dem Spieler einen Punkt
                        this.player[this.state.player].points += 1;
                        // füge die aufgedeckten Karten zu den gefundenen Karten hinzu
                        this.openCards.forEach(card => this.foundCards.push(card))
                        console.log('new found cards:', this.foundCards.map(it => it.name))
                    } else {
                        //Karten passen nicht zusammen
                        this.missmatchedCards = this.openCards
                        // select next player
                        this.state.player = (this.state.player + 1) % this.numberOfPlayers
                
                    }
                    if (this.foundCards.length >= this.numberOfCards*2) {
                        //alle Karten wurden aufgedeckt -> beende das spiel
                        this.state = {type: 'end'}
                        // behandle den neuen Spielzustand
                        this.gameUpdate();
                    }
                    // drehe die Karten wieder um
                    this.openCards = []
                }, 1000);
                return
            }
            if (this.state.type === 'end') {
                //sortiere Spieler nach ihren Punkten
                const playersByPoints = Object.entries(this.player)
                    .sort((a,b) => this.player[b[0]].points - this.player[a[0]].points )
                console.log(playersByPoints)

                if (playersByPoints[0][1].points == playersByPoints[1][1].points) {
                    this.state.winner = undefined;
                    this.state.kind = 'tie';
                } else {
                    //Gewinner ist der Spieler mit den meisten Punkten
                    this.state.winner = playersByPoints[0][1].name;
                    this.state.kind = 'win';
                }
            }
        },
        shuffle() {
            // mische die Karten
            this.cards = _.shuffle(this.cards)
        },
        openCard(card) {
            // ist der aktuelle Spielzustand nicht 'select' -> Karten auswählen ist nicht möglich
            if (this.state.type !== 'select') return;

            // sind bereits 2 Karten umgedreht? -> verhindere, dass weitere Karten umgedreht werden können
            if (this.openCards.length > 1) return;

            // ist die Karte schon umgedreht?
            if (this.openCards.includes(card)) {
                return
            }
            
            console.log(`add card ${card.name}`);
            
            // drehe die Karte um
            this.openCards.push(card)

            // sind schon 2 Karten umgedreht?
            if (this.openCards.length > 1) {
                // update den Spielzustand
                this.gameUpdate()
            }
            console.log("current cards", this.openCards.map(card => card.name))
        },
    },
    computed: {
        humanDuration() {
            const pad = str =>(str.length == 1) ? "0" + str  : str;

            const minutes = Math.round(this.currentDuration / 60) + "";
            const seconds = Math.round(this.currentDuration % 60) + "";

            return pad(minutes) + ":" + pad(seconds);
        }
    }
})
